﻿using System;


namespace TDD2021
{
    // 1. Новый код пишется только тогда, 
    //когда будет написан юнит-тест, который не проходит
    // 2. Вы пишите только тот код, который нужен, чтобы тест проходил
    //3. Вы пишите минимальный тест, который не пройдет
    public class MyList<T>
    {
        private T[] _items;
        private int count;

        public int Capacity { get; private set; }
        public int Count
        {
            get => count; private set
            {
                count = value;

                if (count >= Capacity)
                {
                    Capacity *= 2;
                    var temp = new T[Capacity];
                    Array.Copy(_items, temp, Count);
                    _items = temp;
                }
            }
        }

        public MyList()
        {
            Capacity = 4;
            Count = 0;
            _items = new T[Capacity];
        }

        public void Add(T item)
        {
            _items[Count] = item;
            Count++;
        }
        public void Insert(int index, T item)
        {
            if (index < 0 || index > Count) throw new ArgumentOutOfRangeException();

            Array.Copy(_items, index, _items, index + 1, Count - index);
            _items[index] = item;
            Count++;
        }
        public void RemoveAt(int index)
        {
            if (index < 0 || index >= Count) throw new ArgumentOutOfRangeException();

            Array.Copy(_items, index + 1, _items, index, Count - index);
            Count--;
        }

        public T this[int index]
        {
            get
            {
                if (index < 0 || index >= Count) throw new ArgumentOutOfRangeException();
                return _items[index];
            }
            set
            {
                if (index < 0 || index >= Count) throw new ArgumentOutOfRangeException();
                _items[index] = value;
            }
        }
    }
}
