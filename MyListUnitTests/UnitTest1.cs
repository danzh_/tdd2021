﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using TDD2021;

namespace TDDTesting
{
    [TestClass]
    public class MyListUnitTest
    {
        [TestMethod]
        public void CountMustBeZeroAfterListCreation()
        {
            var list = new MyList<int>();
            int expected = 0;
            Assert.AreEqual(expected, list.Count);
        }

        [TestMethod]
        public void CapacityMustBeFourAfterListCreation()
        {
            var list = new MyList<int>();
            int expected = 4;
            Assert.AreEqual(expected, list.Capacity);
        }
        [TestMethod]
        public void ItemExistsAfterAdding()
        {
            var list = new MyList<int>();
            int expected = 3;
            list.Add(expected);
            Assert.AreEqual(expected, list[0]);
        }
        [TestMethod]
        public void CountIncreasedAfterAdding()
        {
            var list = new MyList<int>();
            int expected = 1;
            list.Add(expected);
            Assert.AreEqual(expected, list.Count);
        }
        [TestMethod]
        public void ItemsExistsAfterAdding()
        {
            var list = new MyList<int>();
            int count = 100;
            for (int i = 0; i < count; i++)
                list.Add(i);
            Assert.AreEqual(count, list.Count);
            for (int i = 0; i < count; i++)
            {
                int expected = i;
                Assert.AreEqual(expected, list[i]);
            }
        }
        [TestMethod]
        public void ItemExistsAfterInserting()
        {
            var list = new MyList<int>();
            int count = 7;
            for (int i = 0; i < count; i++)
                list.Add(i);
            int expected = 101;
            list.Insert(0, expected);
            Assert.AreEqual(expected, list[0]);
            for (int i = 1; i < count + 1; i++)
            {
                expected = i - 1;
                Assert.AreEqual(expected, list[i]);
            }
        }
        [TestMethod]
        public void CountIncreasedAfterInserting()
        {
            var list = new MyList<int>();
            int count = 100;
            for (int i = 0; i < count; i++)
                list.Add(i);
            int expected = 101;
            list.Insert(0, expected);
            Assert.AreEqual(expected, list[0]);
            for (int i = 1; i < count + 1; i++)
            {
                expected = i - 1;
                Assert.AreEqual(expected, list[i]);
            }
            Assert.AreEqual(count + 1, list.Count);
        }
        [TestMethod]
        [ExpectedException(typeof(ArgumentOutOfRangeException))]
        public void ExceptionIfInsertAtNegativeIndex()
        {
            var list = new MyList<int>();
            list.Insert(-1, 1);
        }
        [TestMethod]
        [ExpectedException(typeof(ArgumentOutOfRangeException))]
        public void ExceptionIfInsertAtIndexThatMoreThanCount()
        {
            var list = new MyList<int>();
            list.Insert(1, 1);
        }
        [TestMethod]
        public void CanInsertToLastPosition()
        {
            var list = new MyList<int>();
            list.Insert(0, 1);
            Assert.AreEqual(1, list[0]);
        }
        [TestMethod]
        public void CapacityIncreasedAfterInserting()
        {
            var list = new MyList<int>();
            int count = 7;
            for (int i = 0; i < count; i++)
            {
                list.Add(i);
                list.Insert(0, i);
            }
            Assert.AreEqual(count * 2, list.Count);
            Assert.AreEqual(16, list.Capacity);
        }
        [TestMethod]
        [ExpectedException(typeof(ArgumentOutOfRangeException))]
        public void ExceptionIfTakeItemAtIndexMoreThanCount()
        {
            var list = new MyList<int>();
            list.Add(99);
            list[1] = 1;
        }

        [TestMethod]
        public void CheckIfElementRemoved()
        {
            var list = new MyList<int>();
            int removeIndex = 3;
            int n = 10;
            for (int i = 0; i < n; i++)
            {
                list.Add(i);
            }
            list.RemoveAt(removeIndex);
            for (int i = 0; i < n - 1; i++)
            {
                if (i < removeIndex)
                {
                    Assert.AreEqual(i, list[i]);
                }
                else
                {
                    Assert.AreEqual(i + 1, list[i]);
                }
            }
        }

        [TestMethod]
        public void CheckIfCountDecreased()
        {
            var list = new MyList<int>();
            int removeIndex = 3;
            int n = 10;
            for (int i = 0; i < n; i++)
            {
                list.Add(i);
            }
            list.RemoveAt(removeIndex);
            Assert.AreEqual(n - 1, list.Count);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentOutOfRangeException))]
        public void ExceptionIfRemoveAtNegativeIndex()
        {
            var list = new MyList<int>();
            int removeIndex = -1;
            int n = 10;
            for (int i = 0; i < n; i++)
            {
                list.Add(i);
            }
            list.RemoveAt(removeIndex);
        }
        [TestMethod]
        [ExpectedException(typeof(ArgumentOutOfRangeException))]
        public void ExceptionIfRemoveAtTooBigIndex()
        {
            var list = new MyList<int>();
            int removeIndex = 1000;
            int n = 10;
            for (int i = 0; i < n; i++)
            {
                list.Add(i);
            }
            list.RemoveAt(removeIndex);
        }
    }
}
